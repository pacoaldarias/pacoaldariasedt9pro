/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.modelo;

/**
 *
 * @author alumno
 */
public class Interino extends Profesor {

    private String destino;

    public Interino(String id, String nombre, String idp, int horas, String destino) {
        super(id, nombre, idp, horas);
        this.destino = destino;

    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Override
    public void setHoraslectivas(int horaslectivas) {
        this.horaslectivas = 10;
    }

    public String toString() { // Sobreescritura del método
        return super.toString() + "Destino: " + destino;
    }

}
