/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.modelo;

/**
 *
 * @author alumno
 */
public class Titular extends Profesor {

    private String nrp;
    private String destino;

    public Titular(String id, String nombre, String idp, int horas, String nrp, String destino) {
        super(id, nombre, idp, horas);

        this.nrp = nrp;
        this.destino = destino;
    }

    /**
     * @return the nrp
     */
    public String getNrp() {
        return nrp;
    }

    /**
     * @param nrp the nrp to set
     */
    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Override
    public void setHoraslectivas(int horaslectivas) {
        this.horaslectivas = 20;
    }

    public String toString() { // Sobreescritura del método
        return super.toString().concat("Nrp: ").concat(nrp).
                concat("Destino: ").concat(destino);
    }

}
