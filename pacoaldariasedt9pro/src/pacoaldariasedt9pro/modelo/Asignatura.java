/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.modelo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Asignatura {

    private String ida;
    private String nombre;
    private int horas;
    private Profesor profesor;

    public Asignatura(String ida, String nombre, Profesor profesor, int horas) {
        this.ida = ida;
        this.nombre = nombre;
        this.profesor = profesor;
        this.horas = horas;
    }

    /**
     * @return the ida
     */
    public String getIda() {
        return ida;
    }

    /**
     * @param ida the ida to set
     */
    public void setIda(String ida) {
        this.ida = ida;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the profesor
     */
    public Profesor getProfesor() {
        return profesor;
    }

    /**
     * @param profesor the profesor to set
     */
    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public String toString() { // Sobreescritura del método
        return "Ida: " + ida + " Nombre: " + nombre + " Profesor " + profesor;
    }

    /**
     * @return the horas
     */
    public int getHoras() {
        return horas;
    }

    /**
     * @param horas the horas to set
     */
    public void setHoras(int horas) {
        this.horas = horas;
    }

}
