/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.modelo;

/**
 *
 * @author alumno
 */
public abstract class Profesor extends Persona {

    protected String idp;
    protected int horaslectivas;

    public Profesor(String id, String nombre, String idp, int horas) {
        super(id, nombre);
        this.idp = idp;
        this.horaslectivas = horas;

    }

    /**
     * @return the idp
     */
    public String getIdp() {
        return idp;
    }

    /**
     * @param idp the idp to set
     */
    public void setIdp(String idp) {
        this.idp = idp;
    }

    /**
     * @return the horaslectivas
     */
    public int getHoraslectivas() {
        return horaslectivas;
    }

    /**
     * @param horaslectivas the horaslectivas to set
     */
    public abstract void setHoraslectivas(int horaslectivas);

    public String toString() { // Sobreescritura del método
        return super.toString().concat("Idp: ").concat(idp).
                concat("Horas lectivas: " + horaslectivas);
    }

}
