/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.datos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import pacoaldariasedt9pro.modelo.Asignatura;
import pacoaldariasedt9pro.modelo.Interino;
import pacoaldariasedt9pro.modelo.Titular;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Fichero implements IDatos {

    private File finterino;
    private File ftitular;
    private File fasignatura;
    private int idi = 0;
    private int idt = 0;
    private int ida = 0;
    private static String interinos = "interinos.cvs";
    private static String titulares = "titulares.cvs";
    private static String profesores = "titulares.cvs";

    @Override
    public void create(Titular titular) {

        idt++;
        titular.setId("" + idt);
        FileWriter fw = null;
        try {
            fw = new FileWriter(ftitular, true);
            grabarTitular(titular, fw);
            fw.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public ArrayList read() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Titular a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Titular a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(Interino interino) {
        idi++;
        interino.setId("" + idi);
        FileWriter fw = null;
        try {
            fw = new FileWriter(finterino, true);
            grabarInterino(interino, fw);
            fw.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public void update(Interino interino) {
        idi++;
        interino.setId("" + idi);
        FileWriter fw = null;
        try {
            fw = new FileWriter(finterino, true);
            grabarInterino(interino, fw);
            fw.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public void delete(Interino g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(Asignatura asignatura) {
        ida++;
        asignatura.setIda("" + ida);
        FileWriter fw = null;
        try {
            fw = new FileWriter(finterino, true);
            grabarAsignatura(asignatura, fw);
            fw.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public void update(Asignatura a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Asignatura a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void instalar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void desinstalar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void grabarTitular(Titular titular, FileWriter fw) throws IOException {

        fw.write(titular.getId() + "");
        fw.write(";");
        fw.write(titular.getNombre());
        fw.write(";");
        fw.write("\r\n");
    }

    private void grabarInterino(Interino interino, FileWriter fw) throws IOException {

        fw.write(interino.getId() + "");
        fw.write(";");
        fw.write(interino.getNombre());
        fw.write(";");
        fw.write("\r\n");
    }

    private void grabarAsignatura(Asignatura asignatura, FileWriter fw) throws IOException {

        fw.write(asignatura.getIda() + "");
        fw.write(";");
        fw.write(asignatura.getNombre());
        fw.write(";");
        fw.write("\r\n");
    }

}
