/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.datos;

import java.util.ArrayList;
import pacoaldariasedt9pro.modelo.Asignatura;
import pacoaldariasedt9pro.modelo.Interino;
import pacoaldariasedt9pro.modelo.Titular;

/**
 *
 * @author alumno
 */
public interface IDatos {

    //
    public void create(Titular a); // Crea uno nuevo

    public ArrayList read(); // Obtiene todos

    public void update(Titular a); // Actuzaliza uno

    public void delete(Titular a);  // Borrar uno

    //
    public void create(Interino g); // Crea uno nuevo

    public void update(Interino g); // Actuzaliza uno

    public void delete(Interino g);  // Borrar uno

    //
    public void create(Asignatura a); // Crea uno nuevo

    public void update(Asignatura a); // Actuzaliza uno

    public void delete(Asignatura a);  // Borrar uno

    //
    public void instalar();  // Instalar Ficheros/ BDR / BDOO

    public void desinstalar();  // Desistalar  Ficheros/ BDR / BDOO

}
