package pacoaldariasedt9pro.controlador;

import java.util.ArrayList;
import java.util.Iterator;
import pacoaldariasedt9pro.datos.IDatos;
import pacoaldariasedt9pro.modelo.Asignatura;
import pacoaldariasedt9pro.modelo.Interino;
import pacoaldariasedt9pro.modelo.Profesor;
import pacoaldariasedt9pro.modelo.Titular;
import pacoaldariasedt9pro.vista.VistaTerminal;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Listin {

	private ArrayList<Profesor> listinProfesores;
	private ArrayList<Asignatura> listinAsignaturas;
	private IDatos modelo;
	private VistaTerminal vista;

	public Listin(IDatos modelo, VistaTerminal vista) {
		listinProfesores = new ArrayList<Profesor>();
		listinAsignaturas = new ArrayList<Asignatura>();
		this.vista = (VistaTerminal) vista;
		this.modelo = modelo;
	}

	public void mostrar() {
		Profesor profesor;
		Asignatura asignatura;
		String tmpStr1;
		Iterator<Profesor> itp = listinProfesores.iterator();
		Iterator<Asignatura> ita = listinAsignaturas.iterator();

		while (itp.hasNext()) { // Para cada profesor

			profesor = itp.next();

			if (profesor instanceof Interino) {
				tmpStr1 = "Interino";
			} else {
				tmpStr1 = "Titular";
			}
			vista.mostrar("Profesor  Id:" + profesor.getId() + " Nombre:" + profesor.getNombre());
			vista.mostrar(" Tipo:" + tmpStr1 + ". Horas de este profesor: " + (profesor.getHoraslectivas()));
			vista.mostrar("\n");

			int contarasignaturas = 0;
			int horasasignaturas = 0;
			while (ita.hasNext()) { // Para cada asignatura del profesor
				asignatura = ita.next();
				if (asignatura.getProfesor().getIdp().equals(profesor.getIdp())) {
					contarasignaturas++;
					horasasignaturas = horasasignaturas + asignatura.getHoras();
					vista.mostrar("  Asigntura id:" + asignatura.getIda()
							  + " nombre:" + asignatura.getNombre()
							  + " horas:" + asignatura.getHoras()
					);
					vista.mostrar("\n");
				}

			}
			vista.mostrar("  Asignaturas " + contarasignaturas + " "
					  + "Horas  " + horasasignaturas);
			vista.mostrar("\n");
		}
	}

	public void horas() {
		int total = 0;
		int cont = 0;
		Iterator<Profesor> it = listinProfesores.iterator();
		while (it.hasNext()) {
			total = it.next().getHoraslectivas() + total;
			cont++;
		}

		vista.mostrar("Total Profesores: " + cont + " Horas Todos profesores: " + total);
		vista.mostrar("\n");
	}

	public void crea() {

		Interino pi1 = new Interino("id1", "Paco", "p1", 19, "ceed");
		Titular pt1 = new Titular("id2", "Juan", "p2", 20, "rp1", "ceed");
		listinProfesores.add(pi1);
		listinProfesores.add(pt1);

		Asignatura a1 = new Asignatura("a1", "ED", pi1, 4);
		Asignatura a2 = new Asignatura("a2", "DWS", pi1, 8);
		listinAsignaturas.add(a1);
		listinAsignaturas.add(a2);

	}

	void grabar() {

		Profesor profesor;
		Asignatura asignatura;
		String tmpStr1;
		Iterator<Profesor> itp = listinProfesores.iterator();
		Iterator<Asignatura> ita = listinAsignaturas.iterator();

		while (itp.hasNext()) { // Para cada profesor

			profesor = itp.next();

			if (profesor.getIdp().equals(profesor)) {
				vista.mostrar(profesor.toString());
			}
			if (profesor instanceof Interino) {
				tmpStr1 = "Interino";
				modelo.create((Interino) profesor);
			} else {
				tmpStr1 = "Titular";
				modelo.create((Titular) profesor);
			}

			int contarasignaturas = 0;
			int horasasignaturas = 0;
			while (ita.hasNext()) { // Para cada asignatura del profesor
				asignatura = ita.next();
				if (asignatura.getProfesor().getIdp().equals(profesor.getIdp())) {
					contarasignaturas++;
					horasasignaturas = horasasignaturas + asignatura.getHoras();
					modelo.create(asignatura);

				}

			}

		}
	}

}
