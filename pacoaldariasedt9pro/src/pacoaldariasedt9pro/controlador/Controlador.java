/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt9pro.controlador;

import pacoaldariasedt9pro.datos.IDatos;
import pacoaldariasedt9pro.vista.VistaTerminal;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Controlador {

    VistaTerminal vista;
    IDatos modelo;

    public Controlador(IDatos modelo, VistaTerminal vista) {
        this.modelo = modelo;
        this.vista = vista;
        inicio();
    }

    private void inicio() {

        Listin listin = new Listin(modelo, vista);
        listin.crea();
        listin.mostrar();
        listin.horas();
        //listin.grabar();

    }

}
